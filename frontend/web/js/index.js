var geolocationMap, dir;
window.onload = function() {
    var popup = L.popup();
    geolocationMap = L.map('mapid', {
        layers: MQ.mapLayer(),
        center: [3.517350, 98.609770],  //location RS Adam Malik
        zoom: 12
    });
  
    function geolocationErrorOccurred(geolocationSupported, popup, latLng) {
        popup.setLatLng(latLng);
        popup.setContent(geolocationSupported ?
                '<b>Error:</b> The Geolocation service failed.' :
                '<b>Error:</b> This browser doesn\'t support geolocation.');
        popup.openOn(geolocationMap);
    }

    geolocationErrorOccurred(false, popup, geolocationMap.getCenter());

    placeSearch({
        key: 'jR5CEizgWtJAikvfFVVbjX26wBSsOPNf',
        container: document.querySelector('#place-search-input'),
        useDeviceLocation: true,
        collection: [
            'adminArea',
        ]
    });
}

    function getDistanceByCoordinate() {
        dir = MQ.routing.directions();
        dir.route({
            locations: [
                '3.517350,98.609770', //location RS Adam Malik
                'Sei Agul'
            ],
        });
        geolocationMap.addLayer(MQ.routing.routeLayer({
            directions: dir,
            fitBounds: true
        }));
        

        dir.on('success', function (data) {
            console.log(data.route.locations);
            console.log("Rp. " + parseInt(parseFloat(data.route.distance) * 20000));
            console.log(data.route.from);
            $('#pesanambulan-alamat').val(data.route.locations[1].street + " " + data.route.locations[1].adminArea5);
            $('#pesanambulan-jarak').val(parseFloat(data.route.distance).toFixed(2) + " KM");
            $('#pesanambulan-harga').val("Rp. " + parseInt(parseFloat(data.route.distance) * 20000));
        });
    }