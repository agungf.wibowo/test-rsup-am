function searchLocation() {
    var dir = MQ.routing.directions();
    if($('#pesanambulan-alamat').val() == '') {
        dir.route({
            locations: [
                '3.517350,98.609770', //location RS Adam Malik
                'Sei Agul'
            ],
        });
    } else {
        dir.route({
            locations: [
                '3.517350,98.609770',
                $('#pesanambulan-alamat').val()
            ]
        });
    }
    geolocationMap.addLayer(MQ.routing.routeLayer({
        directions: dir,
        fitBounds: true
    }));
    // console.log(latLng.toString())
    dir.on('success', function (data) {
        console.log(data.route.locations);
        console.log("Rp. " + parseInt(parseFloat(data.route.distance) * 20000));
        console.log(data.route.distance);
        $('#form-address-location').attr('hidden', false);
        $('#pesanambulan-alamat').val(data.route.locations[1].street + " " + data.route.locations[1].adminArea5);
        $('#pesanambulan-jarak').val(parseFloat(data.route.distance).toFixed(2));
        $('#pesanambulan-harga').val(Math.floor(parseFloat(data.route.distance) * 20) * 1000);
    });
}