<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/leaflet.css',
        'https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i"',
        // 'vendors/bootstrap/css/bootstrap.min.css',
        'vendors/icofont/icofont.min.css',
        'vendors/boxicons/css/boxicons.min.css',
        'vendors/venobox/venobox.css',
        'vendors/animate.css/animate.min.css',
        'vendors/remixicon/remixicon.css',
        'vendors/owl.carousel/assets/owl.carousel.min.css',
        'vendors/bootstrap-datepicker/css/bootstrap-datepicker.min.css',
        'css/style.css',
    ];
    public $js = [
        'vendors/jquery/jquery.min.js',
        'vendors/bootstrap/js/bootstrap.bundle.min.js',
        'vendors/jquery.easing/jquery.easing.min.js',
        'vendors/php-email-form/validate.js',
        'vendors/venobox/venobox.min.js',
        'vendors/waypoints/jquery.waypoints.min.js',
        'vendors/counterup/counterup.min.js',
        'vendors/owl.carousel/owl.carousel.min.js',
        'vendors/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
        'js/main.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
