<?php

/* @var $this yii\web\View */

$this->title = 'Home';
$this->registerJsFile(
  'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/leaflet.js',
  ['depends' => [\yii\web\JqueryAsset::className()]]
);

$this->registerJsFile(
  'https://www.mapquestapi.com/sdk/leaflet/v2.s/mq-map.js?key=jR5CEizgWtJAikvfFVVbjX26wBSsOPNf',
  ['depends' => [\yii\web\JqueryAsset::className()]]
);

$this->registerJsFile(
  'https://www.mapquestapi.com/sdk/leaflet/v2.s/mq-routing.js?key=jR5CEizgWtJAikvfFVVbjX26wBSsOPNf',
  ['depends' => [\yii\web\JqueryAsset::className()]]
);

$this->registerJsFile(
  "@web/js/index.js",
  ['depends' => [\yii\web\JqueryAsset::className()]]
)
?>
<!-- ======= Why Us Section ======= -->
<section id="why-us" class="why-us">
  <div class="container">

    <div class="row">
      <div class="col-lg-4 d-flex align-items-stretch">
        <div class="content">
          <h3>Why Choose Medilab?</h3>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Duis aute irure dolor in reprehenderit
            Asperiores dolores sed et. Tenetur quia eos. Autem tempore quibusdam vel necessitatibus optio ad corporis.
          </p>
          <div class="text-center">
            <!-- <a href="#" class="more-btn">Learn More <i class="bx bx-chevron-right"></i></a> -->
          </div>
        </div>
      </div>
      <div class="col-lg-8 d-flex align-items-stretch">
        <div class="icon-boxes d-flex flex-column justify-content-center">
          <div class="row">
            <div class="col-xl-4 d-flex align-items-stretch">
              <div class="icon-box mt-4 mt-xl-0">
                <i class="bx bx-receipt"></i>
                <h4>Corporis voluptates sit</h4>
                <p>Consequuntur sunt aut quasi enim aliquam quae harum pariatur laboris nisi ut aliquip</p>
              </div>
            </div>
            <div class="col-xl-4 d-flex align-items-stretch">
              <div class="icon-box mt-4 mt-xl-0">
                <i class="bx bx-cube-alt"></i>
                <h4>Ullamco laboris ladore pan</h4>
                <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt</p>
              </div>
            </div>
            <div class="col-xl-4 d-flex align-items-stretch">
              <div class="icon-box mt-4 mt-xl-0">
                <i class="bx bx-images"></i>
                <h4>Labore consequatur</h4>
                <p>Aut suscipit aut cum nemo deleniti aut omnis. Doloribus ut maiores omnis facere</p>
              </div>
            </div>
          </div>
        </div><!-- End .content-->
      </div>
    </div>

  </div>
</section><!-- End Why Us Section -->

<!-- ======= Appointment Section ======= -->
<section id="pesan-ambulan" class="appointment section-bg">
  <div class="container">

    <div class="section-title">
      <h2>Pilih Ambulan</h2>
      <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p>
    </div>
    <div class="row">
      <div class="col-12">
        <form action="" method="post" role="form" class="php-email-form">
          <fieldset>
            <legend>Data pasien:</legend>
            <div class="form-row">
              <div class="col-md-6 form-group">
                <label for="nama">Nama Pasien</label>
                <input type="text" name="nama" class="form-control" id="nama" placeholder="Masukan nama pasien" data-rule="minlen:4" data-msg="Tidak boleh kosong">
                <div class="validate"></div>
              </div>
              <div class="col-md-6 form-group">
                <label for="phone">No Hp</label>
                <input type="tel" class="form-control" name="phone" id="phone" placeholder="No hp yang dapat dihubungi" data-rule="minlen:4" data-msg="Tidak boleh kosong">
                <div class="validate"></div>
              </div>
            </div>
            <!-- <div class="form-row">
              <div class="col-12 form-group">
                <select name="doctor" id="doctor" class="form-control">
                  <option value="">Select Doctor</option>
                  <option value="Doctor 1">Doctor 1</option>
                  <option value="Doctor 2">Doctor 2</option>
                  <option value="Doctor 3">Doctor 3</option>
                </select>
                <div class="validate"></div>
              </div>
            </div> -->
            
            <!-- <div class="form-group">
              <textarea class="form-control" name="pesan" rows="5" placeholder="pesan (boleh kosong)"></textarea>
              <div class="validate"></div>
            </div> -->
            <div class="mb-3">
              <div class="loading">Loading</div>
              <div class="error-message"></div>
              <div class="sent-message">Silahkan lanjutkan pembayaran.</div>
            </div>
          </fieldset>
          
          <fieldset>
            <legend>Data Lokasi:</legend>
            <div class="form-row">
              <div class="col-md-6">
                <div id="mapid" class="" style="height:400px"></div>
              </div>
              <div class="col-md-6 form-group">
                <div class="form-row">
                  <div class="col-12 form-group">
                    <label for="lokasi-antar">Lokasi Rumah Sakit</label>
                    <input type="text" name="lokasi-antar" class="form-control" id="lokasi-antar" placeholder="RSU Adam Malik" data-rule="minlen:4" data-msg="Tidak boleh kosong">
                    <div class="validate"></div>
                  </div>
                  <div class="col-12 form-group">
                    <label for="lokasi-jemput">Lokasi Pasien</label>
                    <input type="tel" class="form-control" name="lokasi-jemput" id="lokasi-jemput" placeholder="No hp yang dapat dihubungi" data-rule="minlen:4" data-msg="Tidak boleh kosong">
                    <div class="validate"></div>
                  </div>
                  <div class="col-12 form-group">
                    <label for="distance">Jarak</label>
                    <input type="text" name="distance" class="form-control-plaintext" id="distance" placeholder="RSU Adam Malik" data-rule="minlen:4" data-msg="Tidak boleh kosong" readonly>
                    <div class="validate"></div>
                  </div>
                  <div class="col-12 form-group">
                    <label for="cost">Harga</label>
                    <input type="text" name="cost" class="form-control-plaintext" id="cost" placeholder="RSU Adam Malik" data-rule="minlen:4" data-msg="Tidak boleh kosong" readonly>
                    <div class="validate"></div>
                  </div>
                </div>
              </div>
            </div>
          </fieldset>
          <div class="w-100">
            <div class="row">
              <div class="col-lg-auto">
                </div>
                <div class="col-lg-auto">
                  <button class="btn" type="submit">Kirim</button>
                </div>
              </div>
            </div>
          </form>
          <button class="btn rounded-pill btn-info" onclick="getDistanceByCoordinate()">Dapatkan lokasi</button>
      </div>
    </div>
  </div>
</section><!-- End Appointment Section -->
