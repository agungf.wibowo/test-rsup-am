<?php

use yii\helpers\Html;
use yii\web\JqueryAsset;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model common\models\PesanAmbulan */
/* @var $form yii\widgets\ActiveForm */


    $this->registerJsFile(
        'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/leaflet.js',
        ['depends' => [\yii\web\JqueryAsset::className()]]
    );

    $this->registerJsFile(
        'https://api.mqcdn.com/sdk/place-search-js/v1.0.0/place-search.js',
        ['depends' => [\yii\web\JqueryAsset::className()]]
    );

    $this->registerJsFile(
        'https://www.mapquestapi.com/sdk/leaflet/v2.s/mq-map.js?key=jR5CEizgWtJAikvfFVVbjX26wBSsOPNf',
        ['depends' => [\yii\web\JqueryAsset::className()]]
    );
    
    $this->registerJsFile(
        'https://www.mapquestapi.com/sdk/leaflet/v2.s/mq-routing.js?key=jR5CEizgWtJAikvfFVVbjX26wBSsOPNf',
        ['depends' => [\yii\web\JqueryAsset::className()]]
    );
    
    $this->registerJsFile(
        "@web/js/index.js",
        ['depends' => [\yii\web\JqueryAsset::className()]]
    );

    $this->registerJsFile(
        "@web/js/sLoc.js",
        ['depends' => [\yii\web\JqueryAsset::className()]]
    );
?>

<div class="pesan-ambulan-form">

<div id="mapid" class="" style="height:400px"></div>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>

    <?php // echo $form->field($model, 'tanggal')->textInput() ?>

    <?= $form->field($model, 'alamat')->textInput(['maxlength' => true]) ?>
    
    <?= Html::button('Pilih Lokasi', ['class' => 'btn btn-success', 'onClick' => 'searchLocation()']) ?>

    <?= $form->field($model, 'jarak')->textInput() ?>

    <?= $form->field($model, 'harga')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
