<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%pesan_ambulan}}`.
 */
class m210406_014205_create_pesan_ambulan_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%pesan_ambulan}}', [
            'id' => $this->primaryKey(),
            'nama' => $this->string()->notNull(),
            'tanggal' => $this->date(),
            'alamat' => $this->string(),
            'jarak' => $this->float(),
            'harga' => $this->float()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%pesan_ambulan}}');
    }
}
