<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "pesan_ambulan".
 *
 * @property int $id
 * @property string $nama
 * @property string|null $tanggal
 * @property string|null $alamat
 * @property float|null $jarak
 * @property float|null $harga
 */
class PesanAmbulan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pesan_ambulan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama'], 'required'],
            [['tanggal'], 'safe'],
            [['jarak', 'harga'], 'number'],
            [['nama', 'alamat'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama' => 'Nama',
            'tanggal' => 'Tanggal',
            'alamat' => 'Alamat',
            'jarak' => 'Jarak',
            'harga' => 'Harga',
        ];
    }
}
